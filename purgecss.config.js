module.exports = {
  content: ["src/ui/App.js", "src/api/components/Component.js", "src/api/components/Converter.js", "src/api/components/Transformers.js"],
  css: ["src/styles/tailwind.css"],
};
