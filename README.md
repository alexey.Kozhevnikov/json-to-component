# CourseWork v1.0

This project, created for course university work.
It's aims on creating a library for coverting JSON-description scheme to React-component.

It can be usefull for Web-developers when they needed in fast component changing.

Instruments I use in this project - React, TailWindCss

To start this project you need to clone it from this repository via comand:
```bash
git clone git@gitlab.com:alexey.Kozhevnikov/coursework.git      // If you clone with SSH

or 

git clone https://gitlab.com/alexey.Kozhevnikov/coursework.git      // If you clone with HTTPS
```

Then you need to install all packages used in project. All dependencies you can see in package.json file.
To intall packages use next command: 
```bash
npm install
```

To start project use command: 
```bash
npm start
```

This project has cheap functionality at this moment.

When you start the project you can see two fields: Textarea and render-window.

In the textarea you can write your json that will be rendered in the render-window in real life.

At this moment it has functionality to render:

1. textField
2. dropDownList
3. someCardWithInfo

Styles are configuring by tailwind (poor realization that will be fixed).
All classNames you can see [here](https://tailwindcss.com/docs/installation/).

PurgeCSS is not implemented because of IoC. It will be fixed in future
