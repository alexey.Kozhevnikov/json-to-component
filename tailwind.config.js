module.exports = {
  theme: {
    extend: {
      borderRadius: {
        "0": "0",
        "1": "0.125rem",
        "2": "0.25rem",
        "3": "0.375rem",
        "4": "0.5rem",
        "5": "0.625rem",
        "6": "0.75rem",
        "7": "0.875rem",
        "8": "1rem",
        "9": "1.25rem",
        "10": "1.5rem",
      },
      fontSize: {
        "1": "0.75rem",
        "2": "0.875rem",
        "3": "1rem",
        "4": "1.125rem",
        "5": "1.25rem",
        "6": "1.5rem",
        "7": "1.875rem",
        "8": "2.25rem",
        "9": "3rem",
        "10": "4rem",
      },
      spacing: {
        "7": "1.75rem",
        "9": "2.25rem",
        "11": "2.75rem",
        "13": "3.25rem",
        "14": "3.5rem",
        "15": "3.75rem",
        "17": "4.25rem",
        "18": "4.5rem",
        "19": "4.75rem",
      },
      minWidth: {
        "1": "0.25rem",
        "2": "0.5rem",
        "3": "0.75rem",
        "4": "1rem",
        "5": "1.25rem",
        "6": "1.5rem",
        "7": "1.75rem",
        "8": "2rem",
        "9": "2.25rem",
        "10": "2.5rem",
        "11": "2.75rem",
        "12": "3rem",
        "13": "3.25rem",
        "14": "3.5rem",
        "15": "3.75rem",
        "16": "4rem",
        "17": "4.25rem",
        "18": "4.5rem",
        "19": "4.75rem",
        "20": "5rem",
        "full": "100%",
        "half": "50%",
        "screen": "100vw"
      },
      minHeight: {
        "1": "0.25rem",
        "2": "0.5rem",
        "3": "0.75rem",
        "4": "1rem",
        "5": "1.25rem",
        "6": "1.5rem",
        "7": "1.75rem",
        "8": "2rem",
        "9": "2.25rem",
        "10": "2.5rem",
        "11": "2.75rem",
        "12": "3rem",
        "13": "3.25rem",
        "14": "3.5rem",
        "15": "3.75rem",
        "16": "4rem",
        "17": "4.25rem",
        "18": "4.5rem",
        "19": "4.75rem",
        "20": "5rem",
        "full": "100%",
        "half": "50%",
        "screen": "100vh"
      },
      maxWidth: {
        "1": "0.25rem",
        "2": "0.5rem",
        "3": "0.75rem",
        "4": "1rem",
        "5": "1.25rem",
        "6": "1.5rem",
        "7": "1.75rem",
        "8": "2rem",
        "9": "2.25rem",
        "10": "2.5rem",
        "11": "2.75rem",
        "12": "3rem",
        "13": "3.25rem",
        "14": "3.5rem",
        "15": "3.75rem",
        "16": "4rem",
        "17": "4.25rem",
        "18": "4.5rem",
        "19": "4.75rem",
        "20": "5rem",
        "full": "100%",
        "half": "50%",
        "screen": "100vw"
      },
      maxHeight: {
        "1": "0.25rem",
        "2": "0.5rem",
        "3": "0.75rem",
        "4": "1rem",
        "5": "1.25rem",
        "6": "1.5rem",
        "7": "1.75rem",
        "8": "2rem",
        "9": "2.25rem",
        "10": "2.5rem",
        "11": "2.75rem",
        "12": "3rem",
        "13": "3.25rem",
        "14": "3.5rem",
        "15": "3.75rem",
        "16": "4rem",
        "17": "4.25rem",
        "18": "4.5rem",
        "19": "4.75rem",
        "20": "5rem",
        "full": "100%",
        "half": "50%",
        "screen": "100vh"
      },
      inset: {
        "0": "0",
      },
    },
  },
  variants: {
    fontStyle: ["responsive", "hover", "focus"],
  },
  plugins: [],
};
