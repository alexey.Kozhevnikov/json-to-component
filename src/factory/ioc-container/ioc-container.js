/*
import {NoDependencyError} from '../../custom-exceptions/ioc-exceptions/no-dep-error'
import {WrongStrategySignatureError} from '../../custom-exceptions/ioc-exceptions/wrong-strategy-signature-error'
import {UnknownIoCError} from '../../custom-exceptions/ioc-exceptions/unknown-ioc-error'
*/
import IoCStorage from '../ioc-container/ioc-storage'

const storage = new IoCStorage()

export function register(key, strategy) {
    if(typeof strategy === 'function' && (strategy.length === 1 || strategy.length === 0))
        storage.set(key, strategy)
    else
        throw new Error("Strategy passed to register function has wrong signature")
}

export function resolve(key, params) {
    const strategy = storage.getOrElse(key, ()=>{throw new Error(`No dependency on key: ${key}`)})
    try {
        return strategy(params)
    } catch(ex) {
        throw new Error(`Error on invocation of strategy on key: ${key}`)
    }
}
