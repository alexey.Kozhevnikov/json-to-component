import {expect} from 'chai'
import 'mocha'

import {resolve, register} from '../ioc-container/ioc-container'

const key = 'test'

describe('IoC Container tests', () => {
    it('can finish successfully', () => {
        var checker = false
        const strategy = (params) => {checker = true}
        register(key, strategy)
        resolve(key, null) 
        expect(checker).equals(true)
    })

    it('works with dependency override', () => {
        var checker = 1
        const strategy1 = (params) => {checker = 2} 
        const strategy2 = (params) => {checker = 3} 

        register(key, strategy1)
        register(key, strategy2)
        resolve(key, null)

        expect(checker).equals(3)
    })

    it('fails if strategy signature is wrong', () => {
        expect(()=>{register(key, 1)}).to.throw("Strategy passed to register function has wrong signature")
    })

    it('fails if no dependency on resolve', () => {
        expect(()=>{resolve(key + 1, [])}).to.throw(`Error on invocation of strategy on key: ${key + 1}`)
    }) 

    it('fails if strategy fails', () => {
        register(key, (params)=>{throw Error()})
        expect(() => {
            resolve(key, 1)
        }).to.throw(`Error on invocation of strategy on key: ${key}`)
    })
})