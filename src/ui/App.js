import React from "react";
import Converter from "../api/components/Converter";
import { Depend } from "../api/components/deps/Dependencies";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      string:
        '{\n"type": "",\n"properties": {},\n"styles": {}\n}',
    };
    this.json = "";
    this.value = {};

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ string: event.target.value });
  }

  render() {
    Depend();
    this.json = this.state.string.replace(/\"/g, '"');
    this.json = "[" + this.json + "]";
    try {
      this.value = JSON.parse(this.json);
    } catch (err) {
      console.log(err);
    }
    return (
      <div className="flex justify-around h-screen">
        <form className="form-container text-field border w-2/5 m-2 resize-none">
          <textarea
            onChange={this.handleChange}
            className="w-full p-1 h-full resize-none"
            value={this.state.string}
            placeholder="Input your JSON here"
          ></textarea>
        </form>
        <div className="w-2/5 border m-2">
          <Converter schema={this.value} />
        </div>
        
      </div>
    );
  }
}

export default App;
