import React from "react";
import { resolve } from "../../factory/ioc-container/ioc-container";

export default class Component extends React.Component {
  render() {
    try {
      let component = resolve(this.props.context.type, this.props.context);
      return component;
    } catch (err) {
      return <div className="text-red-500 font-bold text-center">Error in resolving JSON. Check it</div>;
    }
  }
}
