export const white = () => {
    return "white";
}

export const black = () => {
    return "black";
}

export const red = () => {
    return "red-500";
}

export const blue = () => {
    return "blue-500";
}

export const gray = () => {
    return "gray-500";
}

export const orange = () => {
    return "orange-500";
}

export const yellow = () => {
    return "yellow-500";
}

export const green = () => {
    return "green-500";
}

export const teal = () => {
    return "teal-500";
}

export const indigo = () => {
    return "indigo-500";
}

export const purple = () => {
    return "purple-500";
}

export const pink = () => {
    return "pink-500";
}

export const transparent = () => {
    return "transparent";
}