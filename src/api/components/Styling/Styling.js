import { resolve } from "../../../factory/ioc-container/ioc-container";

const isString = (param) => {
  return typeof param === "string";
};

export const rounded = (scale) => {
  const string = "rounded-" + scale + " ";
  return string;
};

export const border = (array) => {
  //color, style, width
  let styles = "";
  for (let i in array) {
    if (array[i] === 1) {
      styles = styles + "border ";
    }
    try {
      const string = resolve(array[i], null);
      styles = styles + "border-" + string + " ";
    } catch (err) {
      styles = styles + "border-" + array[i] + " ";
    }
  }
  return styles;
};

export const background = (array) => {
  // Поправить эту функцию + сделать последовательное отображение
  //color, size, attachment, repeat, position
  let bground = "";
  if (isString(array)) {
    try {
      const string = resolve(array, null);
      bground = bground + "bg-" + string + " ";
    } catch (err) {
      bground = bground + "bg-" + array + " ";
    }
  } else if (array.length >= 1) {
    for (let i in array) {
      try {
        const string = resolve(array[i], null);
        bground = bground + "bg-" + string + " ";
      } catch (err) {
        bground = bground + "bg-" + array[i] + " ";
      }
    }
  }
  return bground;
};

export const displaying = (param) => {
  return param + " ";
};

export const text_alignment = (param) => {
  const string = "text-" + param + " ";
  return string;
};

export const items_alignment = (param) => {
  const string = "items-" + param + " ";
  return string;
};

export const margin = (array) => {
  const params = array;
  if (params.length >= 4) {
    let string = "";
    string = string + "mt-" + params[0] + " ";
    string = string + "mr-" + params[1] + " ";
    string = string + "mb-" + params[2] + " ";
    string = string + "ml-" + params[3] + " ";
    return string;
  }
  if (params.length === 3) {
    let string = "";
    string = string + "mt-" + params[0] + " ";
    string = string + "mx-" + params[1] + " ";
    string = string + "mb-" + params[2] + " ";
    return string;
  }
  if (params.length === 2) {
    let string = "";
    string = string + "my-" + params[0] + " ";
    string = string + "mx-" + params[1] + " ";
    return string;
  }
  if (params.length === 1) {
    const string = "m-" + params[0] + " ";
    return string;
  } else {
    const string = "m-" + array + " ";
    return string;
  }
};

export const padding = (array) => {
  const params = array;
  if (params.length >= 4) {
    let string = "";
    string = string + "pt-" + params[0] + " ";
    string = string + "pr-" + params[1] + " ";
    string = string + "pb-" + params[2] + " ";
    string = string + "pl-" + params[3] + " ";
    return string;
  }
  if (params.length === 3) {
    let string = "";
    string = string + "pt-" + params[0] + " ";
    string = string + "px-" + params[1] + " ";
    string = string + "pb-" + params[2] + " ";
    return string;
  }
  if (params.length === 2) {
    let string = "";
    string = string + "py-" + params[0] + " ";
    string = string + "px-" + params[1] + " ";
    return string;
  }
  if (params.length === 1) {
    const string = "p-" + params[0] + " ";
    return string;
  } else {
    const string = "p-" + array + " ";
    return string;
  }
};

export const text = (array) => {
  // color, size,
  let styles = "";
  if (isString(array)) {
    const string = resolve(array, null);
    styles = styles + "text-" + string + " ";
  } else {
    for (let i in array) {
      try {
        const string = resolve(array[i], null);
        styles = styles + "text-" + string + " ";
      } catch (err) {
        styles = styles + "text-" + array[i] + " ";
      }
    }
  }
  return styles;
};

export const font = (array) => {
  const param = array;
  let styles = "";
  if (isString(param)) {
    try {
      const string = resolve(array, null);
      styles = styles + "font-" + string + " ";
    } catch (err) {
      styles = styles + "font-" + array + " ";
    }
  }
  else if (param.length >= 1) {
    for (let i in array) {
      try {
        const string = resolve(array[i], null);
        styles = styles + "font-" + string + " ";
      } catch (err) {
        styles = styles + "font-" + array[i] + " ";
      }
    }
  }
  return styles;
};

export const size = (array) => {
  let styles = "";
  const param = array;
  if (param.length >= 2) {
    styles = "w-" + array[0] + " h-" + array[1] + " ";
  } else if (param.length === 1) {
    styles = "w-" + array[0] + " ";
  } else {
    styles = "w-" + array + " ";
  }
  return styles;
};

export const min_size = (array) => {
  let styles = "";
  const param = array;
  if (param.length >= 2) {
    styles = "min-w-" + array[0] + " min-h-" + array[1] + " ";
  } else if (param.length === 1) {
    styles = "min-w-" + array[0] + " ";
  } else {
    styles = "min-w-" + array + " ";
  }
  return styles;
};
