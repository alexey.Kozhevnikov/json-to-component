import React from "react";
import Component from "./Component";

const Converter = (props) => {
  const schemaPull = props.schema;
  return (
    <div>
      {schemaPull.map(schema => <Component context={schema} />)}
    </div>
  );
};

export default Converter;
