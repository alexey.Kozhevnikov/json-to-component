import { register } from "../../../factory/ioc-container/ioc-container";
import * as Transformers from "../Transformers";
import * as Colors from "../Styling/Colors";
import * as Styling from "../Styling/Styling";

export const Depend = () => {
  register("textField", Transformers.TransformTextField);
  register("dropDownList", Transformers.TransformDropDownList);
  register("someCardWithInfo", Transformers.TransformSomeCard);

  register("rounded", Styling.rounded);
  register("border", Styling.border);
  register("background", Styling.background);
  register("displaying", Styling.displaying);
  register("text alignment", Styling.text_alignment);
  register("items alignment", Styling.items_alignment);
  register("margin", Styling.margin);
  register("padding", Styling.padding);
  register("text", Styling.text);
  register("font", Styling.font);
  register("size", Styling.size);
  register("min size", Styling.min_size);

  register("white", Colors.white);
  register("black", Colors.black);
  register("red", Colors.red);
  register("blue", Colors.blue);
  register("gray", Colors.gray);
  register("orange", Colors.orange);
  register("yellow", Colors.yellow);
  register("green", Colors.green);
  register("teal", Colors.teal);
  register("indigo", Colors.indigo);
  register("purple", Colors.purple);
  register("pink", Colors.pink);
  register("transparent", Colors.transparent);
};
