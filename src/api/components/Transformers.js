import React from "react";
import { resolve } from "../../factory/ioc-container/ioc-container";

export const ArraysConcate = (schema) => {
  let finalString = "";
  for (let key in schema) {
    finalString += resolve(key, schema[key]);
    console.log(finalString);
  }
  console.log(finalString);
  return finalString;
};

export const isDefined = (param) => {
  return typeof param === "undefined";
};

export const TransformTextField = (schema) => {
  return (
    <>
      {!isDefined(schema.styles) ? (
        <div className={ArraysConcate(schema.styles.block)}>
          <label className={ArraysConcate(schema.styles.label)}>
            {" "}
            {schema.properties.label}{" "}
          </label>
          <input
            className={ArraysConcate(schema.styles.text)}
            type="text"
            defaultValue={schema.properties.text}
          />
        </div>
      ) : (
        <div className="border border-solid border-black">
          <label> {schema.properties.label} </label>
          <input type="text" defaultValue={schema.properties.text} />
        </div>
      )}
    </>
  );
};

export const TransformDropDownList = (schema) => {
  let options = [];

  for (let i in schema.properties.options) {
    options.push(
      <option value={schema.properties.options[i]}>
        {" "}
        {schema.properties.options[i]}{" "}
      </option>
    );
  }

  return (
    <>
      {!isDefined(schema.styles) ? (
        <div className={ArraysConcate(schema.styles.block)}>
          <label className={ArraysConcate(schema.styles.label)}>
            {" "}
            {schema.properties.label}{" "}
          </label>
          <select className={ArraysConcate(schema.styles.selector)}>
            {options}
          </select>
        </div>
      ) : (
        <div className="border border-solid border-black">
          <label> {schema.properties.label} </label>
          <select>{options}</select>
        </div>
      )}
    </>
  );
};

export const TransformSomeCard = (schema) => {
  return (
    <>
      {!isDefined(schema.styles) ? (
        <div className={ArraysConcate(schema.styles.block)}>
          <p className={ArraysConcate(schema.styles.title)}>
            {" "}
            {schema.properties.title}{" "}
          </p>
          <h2 className={ArraysConcate(schema.styles.head)}>
            {schema.properties.head}
          </h2>
          <p className={ArraysConcate(schema.styles.text)}>
            {" "}
            {schema.properties.text}{" "}
          </p>
        </div>
      ) : (
        <div className="border border-solid border-black">
          <p> {schema.properties.title} </p>
          <h2>{schema.properties.head}</h2>
          <p> {schema.properties.text} </p>
        </div>
      )}
    </>
  );
};
